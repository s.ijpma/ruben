from random import randrange
from tkinter import *
import math
master = Tk()

grid = 400;

w = Canvas(master, 
           width=grid,
           height=grid)
w.pack()

def verycomplicatedfunctiontodeterminewethertodrawornot(x,y):
    res = x^2 + y^2 + 2*x + 2*y + x + y + 400;
    res = res / math.pi;
    res = round(res)
    return (res % 2) == 0; #is even getal?

for x in range(grid):
    for y in range(grid):
        draw = verycomplicatedfunctiontodeterminewethertodrawornot(x,y);
        if (draw):
            if y < grid*0.25:
                w.create_line(x, y, x+1, y+1, fill="#0000FF")
            elif y >= grid*0.25 and y < grid*0.50:     
                w.create_line(x, y, x+1, y+1, fill="#00FF00")
            elif y >= grid*0.50 and y < grid*0.75:     
                w.create_line(x, y, x+1, y+1, fill="#FF0000")
            else:    
                w.create_line(x, y, x+1, y+1, fill="#000000")

mainloop()
